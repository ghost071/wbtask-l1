package main

import (
	"fmt"
	"math/rand"
)

func main() {
	nums := make([]int, 2048)

	for i := range nums {
		nums[i] = i + 1
	}

	// positive
	target := rand.Intn(2048)
	index := search(nums, target)
	fmt.Printf("target: %d, index: %d, nums[index] = %d\n", target, index, nums[index])

	// negative
	target = -target
	index = search(nums, target)
	fmt.Printf("case: value not exist. Res: %d\n", index)

}

func search(nums []int, target int) int {
	min := 0
	max := len(nums) - 1

	mid := (max + 1) / 2
LOOP:
	for {
		switch {
		case mid <= min || mid > max:
			break LOOP

		case target == nums[mid]:
			return mid
		case target > nums[mid]:
			min = mid
			mid = (mid + max + 1) / 2
		default:
			max = mid
			mid = (mid + min) / 2
		}
	}

	if nums[mid] == target {
		return mid
	}

	return -1
}
