package main

import "fmt"

func main() {
	a, b := -7, 15

	// 1
	fmt.Printf("#1 a: %d, b: %d | ", a, b)
	a, b = b, a
	fmt.Printf("a: %d, b: %d\n", a, b)

	// 2
	fmt.Printf("#2 a: %d, b: %d | ", a, b)
	a -= b
	b += a
	a = b - a
	fmt.Printf("a: %d, b: %d\n", a, b)

	// 3
	fmt.Printf("#3 a: %d, b: %d | ", a, b)
	a *= b
	b = a / b
	a /= b
	fmt.Printf("a: %d, b: %d\n", a, b)

	// 4
	fmt.Printf("#4 a: %d, b: %d | ", a, b)
	a ^= b
	b ^= a
	a ^= b
	fmt.Printf("a: %d, b: %d\n", a, b)

}
