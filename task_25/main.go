package main

import "time"

func main() {
	// time.After заблокирует горутину main на указанное время
	Sleep(4 * time.Second)
}

func Sleep(t time.Duration) {
	<-time.After(t)
}
