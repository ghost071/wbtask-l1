package main

import (
	"fmt"
)

// Записывает в первый канал числа
func writeNums(dst chan<- int, source []int) {
	for _, n := range source {
		dst <- n
	}
	return
}

// Читает числа из первого канала и записывает обработанные данные во второй
func writeRes(dst chan<- int, source chan int) {
	for i := 0; i < cap(source); i++ {
		dst <- 2 * <-source
	}
	close(source)
	return
}

// Читает из второго канала, выводит в stdout
func read(source chan int, sig chan<- struct{}) {
	for i := 0; i < cap(source); i++ {
		fmt.Printf("read: %d\n", <-source)
	}
	close(source)
	sig <- struct{}{}
	return
}

func main() {
	arr := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

	ch1 := make(chan int, len(arr))
	ch2 := make(chan int, len(arr))
	sig := make(chan struct{})

	// Все 3 операции запускаются конкурентно
	// Первая функция пишет значение ->
	// вторая разблокировывается обрабатывает, передает значение дальше и снова лочится ->
	// разблокировывается третья выводит значение и снова лочится
	go writeNums(ch1, arr)
	go writeRes(ch2, ch1)
	go read(ch2, sig)

	// Дожидаемся всех горутин
	<-sig
}
