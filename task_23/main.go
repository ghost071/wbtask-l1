package main

import (
	"fmt"
)

func main() {
	arr := []int{1, 2, 3, 4, 5}
	i := 2
	fmt.Printf("%v, cap: %d\n", arr, cap(arr))

	arr1 := append(arr[:i], arr[i+1:]...)
	// arr1 и arr будут ссылать на один и тот же участок памяти
	// если этого нужно избежать, можно воспользоваться copy(dst, src)
	fmt.Printf("%v, cap: %d\n", arr1, cap(arr1))

	// Этот способ не сохранит исходный порядок
	arr[i] = arr[len(arr)-1]
	arr = arr[:len(arr)-2]
	fmt.Println(arr)
}
