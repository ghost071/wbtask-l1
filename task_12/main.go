package main

import "fmt"

type Set struct {
	elements map[string]struct{}
}

// Булево значение указывает был ли добавлен элемент
func (s *Set) Add(key string) bool {
	if _, ok := s.elements[key]; ok {
		return false
	}
	s.elements[key] = struct{}{}
	return true
}

func (s *Set) Delete(key string) {
	delete(s.elements, key)
}

func (s *Set) Lookup() {
	for k, _ := range s.elements {
		fmt.Printf("%s  ", k)
	}
	fmt.Println()
}

func (s *Set) Power() int {
	return len(s.elements)
}

func main() {

	sequence := []string{"cat", "cat", "dog", "cat", "tree"}
	set := &Set{make(map[string]struct{})}

	for _, s := range sequence {
		set.Add(s)
	}
	set.Lookup()
}
