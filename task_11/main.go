package main

import (
	"fmt"
)

func main() {
	// arr1 - первые 185 квадратов, arr2 - первые 16 степеней двойки
	arr1 := make([]int, 185)
	arr2 := make([]int, 16)

	for i := 0; i < 185; i++ {
		if i >= 16 {
			arr1[i] = i * i
			continue
		}
		arr1[i] = i * i
		arr2[i] = 1 << i
	}

	fmt.Println(intersection(arr1, arr2))
}

func intersection(set1, set2 []int) []int {

	has := make(map[int]bool)
	ret := make([]int, 0)

	for _, n := range set1 {
		has[n] = true
	}

	for _, n := range set2 {
		if has[n] {
			ret = append(ret, n)
		}
	}
	return ret
}
