package main

import (
	"fmt"
	"strings"
)

func main() {
	str := "snow dog sun"
	fmt.Printf("%s -> %s\n", str, reverse(str))
}

func reverse(s string) string {
	res := strings.Split(s, " ")

	for i := 0; i < len(res)/2; i++ {
		j := len(res) - i - 1
		res[i], res[j] = res[j], res[i]
	}
	return strings.Join(res, " ")
}
