package main

import (
	"fmt"
	"sync"
)

type ExistErr struct{}

func (e *ExistErr) Error() string {
	return "No data for key"
}

// Mutex можно поместить в структуру и работать с мапой через структуру, что бы не передавать mutex в функции
type syncMap struct {
	sync.RWMutex
	cache map[int]int
}

func (m *syncMap) get(key int) (int, error) {
	// Используется RWMutex и RLock()/RUnlock() для геттера => геттер можно использовать параллельно
	m.RLock()
	data, ok := m.cache[key]
	m.RUnlock()
	if ok {
		return data, nil
	}
	return 0, new(ExistErr)
}

func (m *syncMap) set(key, val int) {
	m.Lock()
	m.cache[key] = val
	m.Unlock()
}

func main() {
	m := &syncMap{sync.RWMutex{}, make(map[int]int)}
	var wg sync.WaitGroup

	wg.Add(20)

	// В циклах вызывается по 10 горутин и неизвестно в каком порядке они отработают
	// Вероятно, чтение по некоторым ключам будет выполняться еще до того, туда было записано значение
	for i := 0; i < 10; i++ {
		go func(i int) {
			defer wg.Done()
			m.set(i, i*10)
			fmt.Printf("write %d: %d\n", i, i*10)
		}(i)
	}

	for i := 0; i < 10; i++ {
		go func(n int) {
			defer wg.Done()
			val, err := m.get(n)
			if err != nil {
				fmt.Printf("%v: %d\n", err, n)
				return
			}
			fmt.Printf("read %d: %d\n", n, val)
		}(i)
	}

	wg.Wait()
}
