package main

import (
	"fmt"
	"sync"
)

func countSquares(arr []int) {
	// Счетчик для ожидания горутин
	wg := &sync.WaitGroup{}

	// На каждой итерации цикла будет создаваться одна горутина, счетчик нужно увеличить на количество запланированных горутин
	wg.Add(len(arr))

	for _, n := range arr {
		go func(x int, wg *sync.WaitGroup) {
			// По завершению работы горутины счетчик должен уменьшиться на единицу
			defer wg.Done()
			fmt.Printf("%d -> %d\n", x, x*x)
		}(n, wg)
	}
	// Дожидаемся выполнения всех горутин (порядок вывода неизвестен)
	wg.Wait()
}

func main() {
	arr := []int{2, 4, 6, 8, 10}
	countSquares(arr)
}
