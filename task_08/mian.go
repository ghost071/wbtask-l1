package main

import "fmt"

func main() {
	var n, i int64
	fmt.Println("Введите число и позицию бита через пробел")
	fmt.Scan(&n, &i)
	fmt.Printf("%d -> %b\n", n, n)
	k := changeBit(n, i)
	fmt.Printf("%d -> %b\n", k, k)
}

// Устанавливает i бит в 1 если он равен 0 и наоборот
func changeBit(n int64, i int64) int64 {

	// i бит = 1
	if t := n & (1 << (i - 1)); t != 0 {
		return n ^ t
	}
	// i бит = 0
	return n | (1 << (i - 1))
}
