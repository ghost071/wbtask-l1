package main

import (
	"fmt"
	"math"
)

type Point struct {
	x float64
	y float64
}

func NewPoint(x, y float64) *Point {
	return &Point{x, y}
}

func (p *Point) GetX() float64 {
	return p.x
}

func (p *Point) GetY() float64 {
	return p.y
}

func distance(a, b *Point) float64 {
	k1 := math.Abs(a.GetX() - b.GetX())
	k2 := math.Abs(a.GetY() - b.GetY())
	return math.Sqrt(k1*k1 + k2*k2)
}

func main() {
	pointA := NewPoint(2, 5)
	pointB := NewPoint(-3, 2)
	fmt.Println(distance(pointA, pointB))
}
