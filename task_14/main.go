package main

import (
	"fmt"
	"reflect"
)

func main() {
	arr := []interface{}{true, 10, make(chan string), "str"}

	for _, val := range arr {
		fmt.Println(defineType(val))
	}
}

func defineType(val interface{}) reflect.Type {
	return reflect.ValueOf(val).Type()
}
