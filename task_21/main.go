package main

import "fmt"

/* Описание проблемы:
   - Сторонний сервис/библиотека имеет необходимый функционал
   - Текущие объекты в проекте не удовлетворяют интерфейсам стороннего сервиса
   - По различным причинам существующие структуры в проекте менять нельзя или нежелательно
   Решение:
   - Создать структуру обертку над существующей структурой, которая реализует нужный интерфейс
   При таком подходе код отвечающий за адаптацию будет отделен от бизнес логики и не повлияет на остальной функционал
*/
//===============================
type DataLoader interface {
	LoadJSON()
}

type JSONData struct {
	content string
}

func (d *JSONData) LoadJSON() {
	//...
	fmt.Println(d.content)
}

//================================

type YAMLData struct {
	content string
}

func (d *YAMLData) LoadYAML() {
	fmt.Println(d.content)
}

type Adapter struct {
	YAMLData *YAMLData
}

// Имитация преобразования
func (a *Adapter) toJSON() {
	a.YAMLData.content = `{"key": "value"}`
}

// Добавляем адаптеру нужный метод, чтобы использовать как интерфейс DataLoader
func (a *Adapter) LoadJSON() {
	a.toJSON()
	a.YAMLData.LoadYAML()
}

type Client struct{}

func (c *Client) getJSON(data DataLoader) {
	data.LoadJSON()
}

func main() {
	client := &Client{}
	data := &YAMLData{content: `- key: value`}
	adapter := &Adapter{data}
	client.getJSON(adapter)
}
