package main

import "fmt"

// Структура human с двумя полями и 2мя методами
type Human struct {
	Name string
	Age  int
}

func (h *Human) DoSomething() {
	fmt.Println("Reading book...")
}

func (h *Human) Present() {
	fmt.Printf("I'm %s, %d years old\n", h.Name, h.Age)
}

// Структура action включает в себя структуру human, и имеет собственный метод DoSomething
type Action struct {
	Human
}

func (a *Action) DoSomething() {
	fmt.Println("Driving car...")
}

func main() {
	// Экземпляр структуры human
	person := Human{Name: "john", Age: 22}

	// Экземпляр структуры action
	action := Action{person}

	// Данный код демонстрирует: несмотря на то, что метод Present() и поля Name, Age
	// не были определены на структуре action, y action есть к ним доступ

	action.Present()                                           // I'm john, 22 years old
	fmt.Printf("Name: %s, Age: %d\n", action.Name, action.Age) // Name: john, Age: 22

	// Обе структуры имеют метод DoSomething()
	fmt.Print("from action: ")
	action.DoSomething() // Driving car...

	fmt.Print("from action.Human: ")
	action.Human.DoSomething() // Reading book...

}
