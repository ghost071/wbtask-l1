package main

import "fmt"

func main() {
	str := "тестовая-string≈"
	fmt.Printf("before: %s | after: %s\n", str, reverse(str))
}

func reverse(s string) string {
	res := []rune(s)

	for i := 0; i < len(res)/2; i++ {
		j := len(res) - i - 1
		res[i], res[j] = res[j], res[i]
	}

	return string(res)
}
