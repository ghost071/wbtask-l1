package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	arr := make([]int, 100000)
	arr1 := make([]int, 12)

	for i := 0; i < 100000; i++ {
		rand.Seed(time.Now().UnixNano())
		arr[i] = rand.Intn(10000)

		if i < 12 {
			arr1[i] = rand.Intn(10)
		}
	}

	start := time.Now()
	qsort(&arr, 0, len(arr)-1)
	dur := time.Since(start)
	fmt.Printf("sorted 100 000 random items in: %v\n\n", dur)

	fmt.Printf("short array for example:\n%v -> ", arr1)
	qsort(&arr1, 0, len(arr1)-1)
	fmt.Println(arr1)
}

func qsort(arr *[]int, lIdx, rIdx int) {
	if len(*arr) < 2 || !(lIdx < rIdx) {
		return
	}

	idx := (rIdx-lIdx)/2 + lIdx
	median := (*arr)[idx]

	i := lIdx
	j := rIdx

	for i <= j {
		if (*arr)[i] <= median {
			i++
		} else if (*arr)[j] >= median {
			j--

		} else {
			(*arr)[i], (*arr)[j] = (*arr)[j], (*arr)[i]
			i++
			j--
		}
	}

	if i > idx {
		(*arr)[idx], (*arr)[j] = (*arr)[j], (*arr)[idx]
		qsort(arr, lIdx, j-1)
		qsort(arr, j+1, rIdx)

	} else {
		(*arr)[idx], (*arr)[i] = (*arr)[i], (*arr)[idx]
		qsort(arr, lIdx, i-1)
		qsort(arr, i+1, rIdx)
	}
}
