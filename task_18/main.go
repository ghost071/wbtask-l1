package main

import (
	"fmt"
	"sync/atomic"
)

type Counter struct {
	val uint64
}

func (c *Counter) Inc() {
	atomic.AddUint64(&c.val, 1)
}

func main() {
	counter := new(Counter)

	// Канал для ожидания всех горутин
	sig := make(chan struct{})

	for i := 0; i < 100000; i++ {
		// Горутина инкрементирует счетчик и отпраляет сигнал о завершении
		go func() {
			counter.Inc()
			sig <- struct{}{}
		}()
	}

	// Ожидание горутин
	for i := 0; i < 100000; i++ {
		<-sig
	}

	fmt.Println(counter.val)
}
