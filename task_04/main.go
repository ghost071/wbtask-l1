package main

import (
	"context"
	"fmt"
	"math/rand"
	"os"
	"os/signal"
	"strconv"
	"time"
)

func handleSignal(cancel context.CancelFunc) {
	sigChan := make(chan os.Signal)
	// Связываем нужные сигналы с каналом. В данном случае отправленный интeррапт будет записан в канал sigChan
	signal.Notify(sigChan, os.Interrupt)

	// Чтение из канала, блокирует функцию до тех пор пока не передадут интеррапт.
	<-sigChan

	// Как только данные будт прочитаны - функция разблокируется
	// Вызвается функция cancel которая закрывает канал связанного с ней контекста
	cancel()
}

// Producer будет писть данные в главный поток
func producer(ctx context.Context, c chan<- int, stopCh chan<- struct{}) {
	for {
		select {
		// Producer использует собственный контекст.
		// Связанная с ним функция отмены будет вызвана, когда завершаться все воркеры
		case <-ctx.Done():
			defer func() {
				stopCh <- struct{}{}
			}()
			return
		default:
			c <- random()
			time.Sleep(time.Second) // Исключительно для наглядности, чтоб успевать читать вывод
		}
	}
}

// Worker ничего не делает пока нечего читать или не отменен контекст
// При отмене контекста посылает сигнал о завершении в stopCh
// При чтении из главного потока выводит свой номер и считанное значение
func worker(ctx context.Context, c <-chan int, stopCh chan<- struct{}, n int) {
	for {
		select {
		case <-ctx.Done():
			fmt.Printf("worker#%d stopped\n", n)
			stopCh <- struct{}{}
			return
		case x := <-c:
			fmt.Printf("from worker#%d: %v\n", n, x)
		default:
			//можно time.Sleep(100 * time.Millisecond) или ничего не делать, переходить к след. итерации
		}
	}
}

func random() int {
	rand.Seed(time.Now().UnixNano())
	return rand.Int()
}

func main() {
	// По умолчанию будет запускаться 5 воркеров
	wrkNums := 5

	if len(os.Args) > 1 {
		n, err := strconv.Atoi(os.Args[1])
		if err != nil {
			fmt.Printf("%v\nUse default value for wrkNum: %d\n", err, wrkNums)
		}
		wrkNums = n
	}

	// stopCh для сигналов о завершенни, mainThread для данных
	stopCh := make(chan struct{}, wrkNums+1)
	mainThread := make(chan int, 100) // Буфер, чтоб в producer селект не лочился, когда worker'ы перестали читать

	// ctx контекст для завершения воркеров при C^c
	// ctxp контекст для продюсера, для завершения после выключения воркеров
	ctx, cancel := context.WithCancel(context.Background())
	ctxp, cancelp := context.WithCancel(context.Background())

	// Создаем горутины для продюсера и воркеров, чтоб передать управление далее
	go producer(ctxp, mainThread, stopCh)

	for i := 0; i < wrkNums; i++ {
		go worker(ctx, mainThread, stopCh, i)
	}

	// Вызов handleSignal блокирует главную горутину, пока не будет получен os.Interrupt
	handleSignal(cancel)
	// Цикл отработает как только будут завершены все воркеры
	for i := 0; i < wrkNums; i++ {
		<-stopCh
	}
	fmt.Println("All workers finished work")
	// Функция отмены для продюсера
	cancelp()
	// Блокируемся (ждем пока соответсвующая ветка select в producer выполнится и отправит сигнал о завершении)
	<-stopCh
	fmt.Println("Producer finished")
	close(mainThread)
}
