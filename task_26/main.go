package main

import (
	"fmt"
	"strings"
)

func main() {
	cases := []string{
		"abcdef",
		"aaba",
		"",
		"ansA",
		"∆∑ø",
		"∑∑",
	}

	for _, s := range cases {
		fmt.Printf("\"%s\" -> %v\n", s, isUniqSymbols(s))
	}
}

func isUniqSymbols(s string) bool {

	runes := []rune(strings.ToLower(s))
	check := make(map[rune]struct{})

	for _, r := range runes {
		if _, ok := check[r]; ok {
			return false
		}
		check[r] = struct{}{}
	}

	return true
}
