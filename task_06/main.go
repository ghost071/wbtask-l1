package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"sync"
	"time"
)

/* Горутину нельзя остановить "снаружи", если внутри самой функции, которая вызывается как горутина
   не реализован специальный механизм и условие для остановки.
   В go обмен данными между горутинами обычно реализуется посредством каналов.
*/

// Имитация работы, для вызова внутри горутин
func doWork(s string, i int) {
	fmt.Printf("from %s: iteration #%d\n", s, i)
	time.Sleep(500 * time.Millisecond)
}

// Горутина будет работать до тех пор, пока в канал "c" никто не пишет
func G1(c <-chan struct{}) {
	for i := 0; ; i++ {
		select {
		case <-c:
			fmt.Println("G1 stopped")
			return
		default:
			doWork("G1", i)
		}
	}
}

// Горутинa будет работать пока не сработает функция cancel для переданного контекста
func G2(ctx context.Context, wg *sync.WaitGroup, fnName string) {
	for i := 0; ; i++ {
		select {
		case <-ctx.Done():
			fmt.Printf("%s stopped\n", fnName)
			wg.Done()
			return
		default:
			doWork(fnName, i)
		}
	}
}

func main() {
	duration := 3 * time.Second

	//=============================================================================================================

	sig := make(chan struct{})

	go G1(sig)

	// В блоке селект main блокируется на заданное время duration
	// По истечению заданного времени пишется значение в канал sig, который с другой стороны читает G1
	select {
	case <-time.After(duration):
		sig <- struct{}{}
	}

	//============================================================================================================

	// Контекст с таймаутом, по истечении заданого времени вызывается функция cancel()
	ctx, cancel := context.WithTimeout(context.Background(), duration)
	// Отложенный вызов, на случай если main завершится быстрее заданного таймаута
	defer cancel()

	wg := &sync.WaitGroup{}
	wg.Add(1)

	go G2(ctx, wg, "G2")
	// Дожидаемся завершения горутины
	wg.Wait()

	//============================================================================================================

	// Функция G2 принимает на вход контекст, ранее был использован контескт с тайаутом.
	// Но можно использовать и другие (withDeadLine и withCancel) не меняя реализацию G2
	// whithcancel удобно использовать когда горутину нужно завершить в связи с определенным событием
	ctx, cancel = context.WithCancel(context.Background())

	// Событие может быть любым, не обязательно из внешней среды.
	// B данном случае используется os.Interrupt
	fmt.Println("Press ctrl+c for stopping G3")

	// Вызывается анонимная горутина, мэйн продожает работу, а анонимная горутина блокируется до тех пор, пока получит Interrupt
	go func(c context.CancelFunc) {
		defer c()
		sig := make(chan os.Signal)
		signal.Notify(sig, os.Interrupt)
		<-sig
		return
	}(cancel)

	wg.Add(1)

	go G2(ctx, wg, "G3")
	wg.Wait()

	//==============================================================================================================

	// Тоже самое что и withtimeout только указывается какое то время в будущем.
	ctx, cancel = context.WithDeadline(context.Background(), time.Now().Add(duration))
	defer cancel()
	wg.Add(1)

	go G2(ctx, wg, "G4")
	wg.Wait()

}
