package main

import (
	"fmt"
	"math/big"
)

func main() {
	var a, b string
	fmt.Print("Num A: ")
	fmt.Scanln(&a)
	fmt.Print("Num B: ")
	fmt.Scanln(&b)

	x, y, k := new(big.Float), new(big.Float), new(big.Float)
	_, ok := x.SetString(a)
	if !ok {
		fmt.Println("Invalid string, set 0 for first num")
	}
	_, ok = y.SetString(b)

	if !ok {
		fmt.Println("Invalid string, set 0 for second num")
	}

	// Add
	fmt.Printf("ADD: %0.2f\n", k.Add(x, y))

	// Multiply
	fmt.Printf("MUL: %0.2f\n", k.Mul(x, y))

	// Sub
	fmt.Printf("SUB: %0.2f\n", k.Sub(x, y))

	// Div
	fmt.Printf("DIV: %0.2f\n", k.Quo(x, y))
}
