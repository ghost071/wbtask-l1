package main

import (
	"context"
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"
)

// Отправляет данные в канал каждую секунду. По истечению контекста отправляет сигнал о завершении работы
func producer(ctx context.Context, c chan<- int, sc chan<- struct{}) {
	for {
		select {
		case <-ctx.Done():
			sc <- struct{}{}
			return
		default:
			c <- random()
			time.Sleep(time.Second)
		}
	}
}

// Читает данные из канала. По истечению контекста отправляет сигнал о завершении
func consumer(ctx context.Context, c <-chan int, sc chan<- struct{}) {
	for {
		select {
		case <-ctx.Done():
			sc <- struct{}{}
			return
		case n := <-c:
			fmt.Println(n)
		default:
		}
	}
}

func random() int {
	rand.Seed(time.Now().UnixNano())
	return rand.Int()
}

func main() {
	// Дефолтный таймаут. Будет использован если иной не задан аргументом командной строки или аргумен некорректный.
	// Для удобства и наглядности таймаут задается в секундах
	timeout := 10 * time.Second

	if len(os.Args) > 1 {
		n, err := strconv.Atoi(os.Args[1])
		if err != nil {
			fmt.Printf("%v\nUse default timeout: %v\n", err, timeout)
		} else {
			timeout = time.Duration(n) * time.Second
		}
	}

	ch := make(chan int)
	stopCh := make(chan struct{})

	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	go producer(ctx, ch, stopCh)
	go consumer(ctx, ch, stopCh)
	// Горутина мейн блокируется до тех пор пока не истечет таймаут и запущенные горутины не отправят сигнал о завершении
	<-stopCh
	<-stopCh
}
