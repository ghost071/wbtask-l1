package main

import "fmt"

func groupVals(vals []float64, interval int) map[int][]float64 {
	groups := make(map[int][]float64)
	for _, n := range vals {
		// Отбрасывается остаток -> результат кратен инервалу
		group := int(n/float64(interval)) * interval
		groups[group] = append(groups[group], n)
	}
	return groups
}

func main() {

	example := []float64{-25.4, -27, 13, 19, 15.5, 24.5, -21, 32.5}
	interval := 10

	fmt.Println(groupVals(example, interval))
}
