package main

import (
	"fmt"
)

func findSquaresSum(args ...int) (sum int) {
	ch := make(chan int, len(args))

	// В первом цикле анонимная вычисляет квадрат аргумента и записывает в канал
	for _, n := range args {
		go func(n int, c chan<- int) {
			c <- n * n
		}(n, ch)
	}

	// На каждой итерации происходит блокировка пока в канал не поступят данные
	for i := 0; i < len(args); i++ {
		sum += <-ch
	}
	close(ch)
	return sum
}

func main() {
	fmt.Println(findSquaresSum(2, 4, 6, 8, 10)) // 220
}
